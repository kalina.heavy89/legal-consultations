<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    private const NAMES = [
        'Admin',
        'Lawyer',
        'Test',
        'Test1',
        'Test2',
        'Test3',
        'Test4',
        'Test5',
        'Test6',
        'Test7',
        'Test8',
        'Test9',
        'Test10',
    ];

    private const EMAILS = [
        'admin@test.com',
        'lawyer@test.com',
        'test.test@test.com',
        'test.test1@test.com',
        'test.test2@test.com',
        'test.test3@test.com',
        'test.test4@test.com',
        'test.test5@test.com',
        'test.test6@test.com',
        'test.test7@test.com',
        'test.test8@test.com',
        'test.test9@test.com',
        'test.test10@test.com',
    ];

    private const PASSWORD = 'Password123';

    private const BUSINESSES = [
        null,
        null,
        'Manufacture of condoms',
        'Trade in moonstones',
        'Some business',
        'Secret business',
        'Almost business',
        'Illegal business',
        'Contraband business',
        'Show business',
        'Hamburger eating business',
        'Suspicious business',
        'Incredible business'
    ];

    private const ROLE_IDS = [
        3,
        2,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $i = 0;

        while (isset(self::NAMES[$i])) {
            DB::table('users')->insert([
                'name' => self::NAMES[$i],
                'email' => self::EMAILS[$i],
                'email_verified_at' => now(),
                'password' => Hash::make(self::PASSWORD),
                'created_at' => now(),
                'updated_at' => now(),
                'business' => self::BUSINESSES[$i],
                'role_id' => self::ROLE_IDS[$i],
                'is_active' => true,
            ]);

            $i++;
        }
    }
}
