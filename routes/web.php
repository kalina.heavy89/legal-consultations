<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Home');
})->name('home');

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::middleware(['client'])->group(function () {
        Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
    });

    Route::get('/calendar', function () {
        return Inertia::render('Calendar');
    })->name('calendar');

    Route::get('/active-matters', function () {
        return Inertia::render('ActiveMatters');
    })->name('active_matters');

    Route::get('/documents', function () {
        return Inertia::render('Documents');
    })->name('documents');

    Route::resource('/users', UserController::class)->only([
        'index',
        'show',
        'update',
    ]);

    Route::middleware(['admin'])->group(function () {
        Route::post('/user-deactivation', [UserController::class, 'deactivate'])
            ->name('user-deactivation');

        Route::post('/user-activation', [UserController::class, 'activate'])
            ->name('user-activation');

        Route::delete('/user-destroy', [UserController::class, 'destroy'])
            ->name('user-destroy');

        Route::post('/user-restoration', [UserController::class, 'restore'])
            ->name('user-restoration');

        Route::delete('/user-ultimately-destroy', [UserController::class, 'ultimatelyDestroy'])
            ->name('user-ultimately-destroy');

        Route::delete('/users-bulk-deletion', [UserController::class, 'bulkDestroy'])
            ->name('users-bulk-deletion');

        Route::post('/users-bulk-deactivation', [UserController::class, 'bulkDeactivate'])
            ->name('users-bulk-deactivation');

        Route::post('/users-bulk-restoration', [UserController::class, 'bulkRestore'])
            ->name('users-bulk-restoration');

        Route::post('/users-bulk-activation', [UserController::class, 'bulkActivate'])
            ->name('users-bulk-activation');

        Route::delete('/users-bulk-ultimately-deletion', [UserController::class, 'bulkUltimatelyDestroy'])
            ->name('users-bulk-ultimately-deletion');
    });

});
