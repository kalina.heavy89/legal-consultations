<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\User\IndexRequest;
use App\Http\Requests\User\UpdateRequest;
use App\Http\Requests\User\UserRequest;
use App\Http\Requests\User\BulkUsersRequest;
use Inertia\Inertia;
use App\Facades\UserFacade;
use App\Facades\RoleFacade;
use App\Models\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param \App\Http\Requests\User\IndexRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index(IndexRequest $request)
    {
        $user = UserFacade::currentUser();

        if ($user->role === User::ROLE_CLIENT) {
            return redirect()->route('dashboard');
        }

        return Inertia::render('Users',[
            'users' => UserFacade::getUsers($request),
            'roles' => RoleFacade::getRolesForSelector(true),
            'statuses' => [
                '',
                'inactive',
                'deleted'
            ],
            'items_on_page' => UserFacade::itemsOnPage(),
            'users_number' => UserFacade::countUsers($request),
            'filters' => $request->query('filters') ? $request->query('filters') : [
                'search' => '',
                'role' => '',
                'status' => '',
            ],
            'bulk_actions' => $request->query('filters') ?
                UserFacade::getBulkActions($request->query('filters')['status']) :
                [
                    '',
                    'deactivate',
                    'delete',
                ],
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Inertia::render('User',[
            'selected_user' => UserFacade::selectedUser($id),
            'roles' => RoleFacade::getRolesForSelector(),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\User\UpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $user = UserFacade::currentUser();

        if ($user->role === User::ROLE_ADMIN) {
            try {
                UserFacade::update($id, [
                    'role_id' => RoleFacade::getRoleIdByName($request->role),
                    'is_active' => $request->is_active,
                ]);
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        }
    }

    /**
     * Deactivate the specified user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deactivate(UserRequest $request)
    {
        UserFacade::deactivate($request->id);

        return redirect()->route('users.index', ['filters' => $request->filters]);
    }

    /**
     * Activate the specified deactivated user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate(UserRequest $request)
    {
        UserFacade::activate($request->id);

        return redirect()->route('users.index', ['filters' => $request->filters]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserRequest $request)
    {
        UserFacade::destroy($request->id);

        return redirect()->route('users.index', ['filters' => $request->filters]);
    }

    /**
     * Restore the specified user from soft deleting.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore(UserRequest $request)
    {
        UserFacade::restore($request->id);

        return redirect()->route('users.index', ['filters' => $request->filters]);
    }

    /**
     * Ultimately destroy the specified user from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ultimatelyDestroy(UserRequest $request)
    {
        UserFacade::ultimatelyDestroy($request->id);

        return redirect()->route('users.index', ['filters' => $request->filters]);
    }

    /**
     * Soft deletes users by the array of ids.
     *
     * @param  \App\Http\Requests\User\BulkUsersRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function bulkDestroy(BulkUsersRequest $request)
    {
        UserFacade::bulkDestroy($request->ids);

        return redirect()->route('users.index', ['filters' => $request->filters]);
    }

    /**
     * Deactivates users by the array of ids.
     *
     * @param  \App\Http\Requests\User\BulkUsersRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function bulkDeactivate(BulkUsersRequest $request)
    {
        UserFacade::bulkDeactivate($request->ids);

        return redirect()->route('users.index', ['filters' => $request->filters]);
    }

    /**
     * Restores deleted users by the array of ids.
     *
     * @param  \App\Http\Requests\User\BulkUsersRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function bulkRestore(BulkUsersRequest $request)
    {
        UserFacade::bulkRestore($request->ids);

        return redirect()->route('users.index', ['filters' => $request->filters]);
    }

    /**
     * Activates deactivated users by the array of ids.
     *
     * @param  \App\Http\Requests\User\BulkUsersRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function bulkActivate(BulkUsersRequest $request)
    {
        UserFacade::bulkActivate($request->ids);

        return redirect()->route('users.index', ['filters' => $request->filters]);
    }

    /**
     * Ultimately deletes users by the array of ids.
     *
     * @param  \App\Http\Requests\User\BulkUsersRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function bulkUltimatelyDestroy(BulkUsersRequest $request)
    {
        UserFacade::bulkUltimatelyDestroy($request->ids);

        return redirect()->route('users.index', ['filters' => $request->filters]);
    }
}
