<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use App\Repositories\BaseRepository;

abstract class BaseService
{
    protected $repo;

    /**
    * Constructor
    *
    * @var BaseRepository $repo
    */
    public function __construct(BaseRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
    * Get all data
    *
    * @return Collection
    */
    public function all(): Collection
    {
        return $this->repo->all();
    }

    /**
    * Create new record
    *
    * @param array $input
    * @return model
    */
    public function create(array $data): Model
    {
        return $this->repo->create($data);
    }

    /**
    * Update data
    *
    * @param integer $id
    * @param array $data
    * @return model
    */
    public function update(string $id, array $data): Model
    {
        return $this->repo->update($id, $data);
    }

    /**
    * Delete record by id
    *
    * @param integer $id
    * @return boolean
    */
    public function destroy(int $id): bool
    {
        return $this->repo->destroy($id);
    }

    /**
    * Find record by id
    *
    * @param int $id
    * @return Model
    */
    public function find(int $id): Model
    {
        return $this->repo->find($id);
    }

    /**
    * Show the first record from the database
    *
    * @return model
    */
    public function first(): Model
    {
        return $this->repo->first();
    }

    /**
    * Show the latest record from the database
    *
    * @return model
    */
    public function last(): Model
    {
        return $this->repo->last();
    }

    /**
    * Get the associated model
    *
    * @return Model
    */
    public function getModel(): Model
    {
        return $this->repo->getModel();
    }

    /**
    * Set the associated model
    *
    * @param $model
    * @return $this
    */
    public function setModel(Model $model)
    {
        $this->repo->setModel($model);
        return true;
    }

    /**
     * count records
     *
     * @return integer
     */
    public function count(string $fieldName, mixed $value): int
    {
        return $this->repo->count($fieldName, $value);
    }

}
