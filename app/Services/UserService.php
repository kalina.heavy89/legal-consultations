<?php

namespace App\Services;

use App\Services\BaseService;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class UserService extends BaseService
{
    /**
    * Constructor.
    *
    * @var UserRepository $repo
    */
    public function __construct(UserRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * Gets the current authenticated user
     *
     * @return array|null
     */
    public function currentUser(): object|null
    {
        return $this->insertRoleName(Auth::user());
    }

    /**
     * Gets the selected user by id
     *
     * @param integer|null $id
     * @return object|null
     */
    public function selectedUser(int|null $id): object|null
    {
        $selectedUser = $this->repo->selectedUser($id);

        if ($selectedUser) {
            return $this->insertRoleName($selectedUser);
        }

        return null;
    }

    /**
     * Inserts the role name to the $user object
     *
     * @param object|null $user
     * @return object|null
     */
    private function insertRoleName(object|null $user): object|null
    {
        if (!$user) {
            return null;
        }

        if ($user->email_verified_at) {
            $user->role = $user->role()->first()->name;
        }

        return $user;
    }

    /**
     * Gets users, appropriate to the role of the current user and filters
     *
     * @param object $request
     * @return array
     */
    public function getUsers(object $request): array
    {
        $currentUser = $this->currentUser();

        if ($currentUser) {
            $request->current_user_role = $currentUser->role;
        }

        return $this->repo->getUsers($request);
    }

    /**
     * Returns ITEMS_ON_PAGE constant value.
     *
     * @return integer
     */
    public function itemsOnPage(): int
    {
        return $this->repo->itemsOnPage();
    }

    /**
     * Counts users, appropriate to the role of the current user and filters
     *
     * @param object $request
     * @return integer
     */
    public function countUsers(object $request): int
    {
        return $this->repo->countUsers($request);
    }

    /**
     * Gets bulk actions depending from selected users status
     *
     * @param string|null $status
     * @return array
     */
    public function getBulkActions(string|null $status): array
    {
        $bulkActions = [];

        switch ($status) {
            case User::STATUS_INACTIVE:
                $bulkActions = [
                    '',
                    'activate',
                    'delete',
                ];

                break;
            case User::STATUS_DELETED:
                $bulkActions = [
                    '',
                    'restore',
                    'ultimately delete',
                ];

                break;
            default:
                $bulkActions = [
                    '',
                    'deactivate',
                    'delete',
                ];
        }

        return $bulkActions;
    }

    /**
     * Deactivate the specified user.
     *
     * @param  int  $id
     * @return object
     */
    public function deactivate(int $id): object
    {
        return $this->update($id, ['is_active' => false]);
    }

    /**
     * Activate the specified deactivated user.
     *
     * @param  int  $id
     * @return object
     */
    public function activate(int $id): object
    {
        return $this->update($id, ['is_active' => true]);
    }

    /**
     * Restore the specified user from soft deleting.
     *
     * @param  int  $id
     * @return void
     */
    public function restore(int $id)
    {
        $this->repo->restore($id);
    }

    /**
     * Ultimately destroy the specified user from storage.
     *
     * @param  int  $id
     * @return void
     */
    public function ultimatelyDestroy(int $id)
    {
        $this->repo->ultimatelyDestroy($id);
    }

    /**
     * Soft deletes users by the array of ids.
     *
     * @param  array  $ids
     * @return void
     */
    public function bulkDestroy(array $ids)
    {
        $this->repo->bulkDestroy($ids);
    }

    /**
     * Deactivates users by the array of ids.
     *
     * @param  array  $ids
     * @return void
     */
    public function bulkDeactivate(array $ids)
    {
        $this->repo->bulkDeactivate($ids);
    }

    /**
     * Restores deleted users by the array of ids.
     *
     * @param  array  $ids
     * @return void
     */
    public function bulkRestore(array $ids)
    {
        $this->repo->bulkRestore($ids);
    }

    /**
     * Activates deactivated users by the array of ids.
     *
     * @param  array  $ids
     * @return void
     */
    public function bulkActivate(array $ids)
    {
        $this->repo->bulkActivate($ids);
    }

    /**
     * Ultimately deletes users by the array of ids.
     *
     * @param  array  $ids
     * @return void
     */
    public function bulkUltimatelyDestroy(array $ids)
    {
        $this->repo->bulkUltimatelyDestroy($ids);
    }
}
