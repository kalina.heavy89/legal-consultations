<?php

namespace App\Services;

use App\Services\BaseService;
use App\Repositories\RoleRepository;

class RoleService extends BaseService
{
    /**
    * Constructor.
    *
    * @var RoleRepository $repo
    */
    public function __construct(RoleRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * Gets id for role by its name
     *
     * @param string|null $name
     * @return integer|null
     */
    public function getRoleIdByName(string|null $name): int|null
    {
        return $this->repo->getRoleIdByName($name);
    }

    /**
     * Gets all existing roles for selector
     *
     * @param boolean $addFirstEmptyValue
     * @return array
     */
    public function getRolesForSelector(bool $addFirstEmptyValue = false): array
    {
        $existingRoles = $this->all();
        $roles = [];

        if ($addFirstEmptyValue) {
            $roles[] = '';
        }

        foreach ($existingRoles as $role) {
            $roles[] = $role->name;
        }

        return $roles;
    }
}
