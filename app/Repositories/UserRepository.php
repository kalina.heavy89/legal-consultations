<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\BaseRepository;
use App\Facades\RoleFacade;

class UserRepository extends BaseRepository
{
    const ITEMS_ON_PAGE = 5;

    /**
    * Constructor.
    *
    * @var User $model
    */
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     * Inits requests for getting users, appropriate to the role of the current user and filters
     *
     * @param object $request
     * @return object
     */
    private function initRequest(object $request)
    {
        $query = $this->model;

        if ($request->current_user_role && $request->current_user_role === 'lawyer') {
            $clientRoleId = RoleFacade::getRoleIdByName('client');
            return $query->where('role_id', $clientRoleId);
        }

        if ($request->filters) {

            if (array_key_exists('status', $request->filters)) {
                $query = $query->when($request->filters['status'], function($query, $status) {
                    if ($status === User::STATUS_DELETED) {
                        $query->onlyTrashed();
                    } else if ($status === User::STATUS_INACTIVE) {
                        $query->where('is_active', false);
                    }
                });

                $query = $query->when(!$request->filters['status'], function($query) {
                    $query->where('is_active', true);
                });
            }

            if (array_key_exists('search', $request->filters)) {
                $query = $query->when($request->filters['search'], function ($query, $search) {
                    $query->where(function ($query) use ($search) {
                        $query->where('name', 'like', "%{$search}%")
                            ->orWhere('email', 'like', "%{$search}%")
                            ->orWhere('business', 'like', "%{$search}%");
                    });
                });
            }

            if (array_key_exists('role', $request->filters)) {
                $query = $query->when($request->filters['role'], function($query, $role) {
                    $roleId = RoleFacade::getRoleIdByName($role);

                    $query->where(function ($query) use ($roleId) {
                        $query->where('role_id', $roleId);
                    });
                });
            }
        } else {
            $query = $query
                ->where('is_active', true);
        }

        return $query;
    }

    /**
     * Gets users
     *
     * @param object $request
     * @return array
     */
    public function getUsers(object $request): array
    {
        $users = $this->initRequest($request)
            ->paginate(self::ITEMS_ON_PAGE)
            ->toArray();

        foreach ($users['data'] as $key => $user) {
            $users['data'][$key]['role'] = RoleFacade::find($user['role_id'])->toArray();
        }

        return $users;
    }

    /**
     * Returns ITEMS_ON_PAGE constant value.
     *
     * @return integer
     */
    public function itemsOnPage(): int
    {
        return self::ITEMS_ON_PAGE;
    }

    /**
     * Counts users
     *
     * @param object $request
     * @return integer
     */
    public function countUsers(object $request): int
    {
        return $this->initRequest($request)->count();
    }

    /**
     * Gets the selected user by id
     *
     * @param integer|null $id
     * @return object|null
     */
    public function selectedUser(int|null $id): object|null
    {
        return $this->model
            ->withTrashed()
            ->find($id);
    }

    /**
     * Restore the specified user from soft deleting.
     *
     * @param  int  $id
     * @return void
     */
    public function restore(int $id)
    {
        $this->model
            ->onlyTrashed()
            ->find($id)
            ->restore();
    }

    /**
     * Ultimately destroy the specified user from storage.
     *
     * @param  int  $id
     * @return void
     */
    public function ultimatelyDestroy(int $id)
    {
        $this->model
            ->where('id', $id)
            ->forceDelete();
    }

    /**
     * Soft deletes users by the array of ids.
     *
     * @param  array  $ids
     * @return void
     */
    public function bulkDestroy(array $ids)
    {
        $this->model
            ->whereIn('id', $ids)
            ->delete();
    }

    /**
     * Deactivates users by the array of ids.
     *
     * @param  array  $ids
     * @return void
     */
    public function bulkDeactivate(array $ids)
    {
        $this->model
            ->whereIn('id', $ids)
            ->update(['is_active' => false]);
    }

    /**
     * Restores deleted users by the array of ids.
     *
     * @param  array  $ids
     * @return void
     */
    public function bulkRestore(array $ids)
    {
        $this->model
            ->onlyTrashed()
            ->whereIn('id', $ids)
            ->restore();
    }

    /**
     * Activates deactivated users by the array of ids.
     *
     * @param  array  $ids
     * @return void
     */
    public function bulkActivate(array $ids)
    {
        $this->model
            ->whereIn('id', $ids)
            ->update(['is_active' => true]);
    }

    /**
     * Ultimately deletes users by the array of ids.
     *
     * @param  array  $ids
     * @return void
     */
    public function bulkUltimatelyDestroy(array $ids)
    {
        $this->model
            ->whereIn('id', $ids)
            ->forceDelete();
    }
}
