<?php

namespace App\Repositories;

use App\Models\Role;
use App\Repositories\BaseRepository;

class RoleRepository extends BaseRepository
{
    /**
    * Constructor.
    *
    * @var Role $model
    */
    public function __construct(Role $model)
    {
        $this->model = $model;
    }

    /**
     * Gets id for role by its name
     *
     * @param string|null $name
     * @return integer|null
     */
    public function getRoleIdByName(string|null $name): int|null
    {
        $result = $this->model
            ->where('name', $name)
            ->first();

        if ($result) {
            return $result->id;
        }

        return null;
    }
}
