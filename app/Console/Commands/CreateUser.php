<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Facades\UserFacade;
use App\Facades\RoleFacade;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class CreateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:user {name?} {email?} {password?} {role?} {business?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a new user with the specified role';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $name = $this->argument('name') ?? '';
        $email = $this->argument('email') ?? '';
        $roleInput = $this->argument('role') ?? '';
        $password = $this->argument('password') ?? '';
        $business = $this->argument('business') ?? '';

        $roles = RoleFacade::all();

        if (empty($name)) {
            $name = $this->ask('Specify the user\'s name');
        }

        if (empty($name)) {
            $this->error('The user\'s name is empty.');

            return Command::FAILURE;
        }

        if (empty($email)) {
            $email = $this->ask('Specify the user\'s email');
        }

        if (empty($email)) {
            $this->error('The user\'s email is empty.');

            return Command::FAILURE;
        }

        if (empty($password)) {
            $password = $this->ask('Specify the user\'s password');
        }

        if (empty($password)) {
            $this->error('The user\'s password is empty.');

            return Command::FAILURE;
        }

        if (empty($roleInput)) {
            $roleInputAsk = 'Specify the user\'s role (';

            foreach ($roles as $key => $role) {
                if ($key > 0) {
                    $roleInputAsk .= ', ';
                }

                $roleInputAsk .= $role->name;
            }

            $roleInputAsk .= ')';

            $roleInput = $this->ask($roleInputAsk, $roles[0]->name);
        }

        $roleId = 0;
        $roleName = '';

        foreach ($roles as $value) {
            if ($roleInput === $value->name) {
                $roleId = $value->id;
                $roleName = $value->name;
                break;
            }
        }

        if ($roleId === 0) {
            $this->error('The user\'s role is invalid.');

            return Command::FAILURE;
        }

        $userData = [
            'name' => $name,
            'email' => $email,
            'password' => $password,
        ];

        Validator::make($userData, [
            'name' => ['required', 'string', 'min:3', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => [
                'required',
                'min:10',
                'regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x]).*$/',
            ],
        ])->validate();

        $userData['password'] = Hash::make($userData['password']);
        $userData['role_id'] = $roleId;

        if ($roleName === 'client') {
            if (empty($business)) {
                $business = $this->ask('Specify the user\'s business', 'Individual');
            }

            $userData['business'] = $business;
        }

        $user = UserFacade::create($userData);
        $user->sendEmailVerificationNotification();

        $this->info('User was successfully created!');

        return Command::SUCCESS;
    }
}
