<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ClearTable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clear:table {table?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clears all rows in the specified table in DB';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $table = $this->argument('table') ?? '';

        if (empty($table)) {
            $table = $this->ask('Specify the table to clear');
        }

        if (empty($table)) {
            $this->error('The table to clear wasn\'t specified');

            return Command::FAILURE;
        }

        DB::table($table)->truncate();

        return Command::SUCCESS;
    }
}
